import 'package:detective/generate_level.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:detective/player.dart';
import 'package:detective/pick_up.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DetectiveGame extends FlameGame
    with PanDetector, TapDetector, KeyboardEvents {
  Vector2 rooms = Vector2(4.0, 3.0);
  late Player player = Player(4);
  Vector2 moving = Vector2.all(0.0);
  int drop_off = 0;
  List<PickUp> objects = [];

  TextComponent roomName = TextComponent(
    textRenderer: TextPaint(
        style: const TextStyle(
      fontSize: 48.0,
      fontFamily: 'Sans Serif',
      color: Colors.deepOrangeAccent,
    )),
  )
    ..position.x = 10
    ..position.y = 10;

  TextComponent playerScore = TextComponent(
    textRenderer: TextPaint(
        style: const TextStyle(
      fontSize: 48.0,
      fontFamily: 'Sans Serif',
      color: Colors.deepOrangeAccent,
    )),
  )
    ..anchor = Anchor.topRight
    ..position.y = 10;

  @override
  Future<void>? onLoad() async {
    await super.onLoad();
    playerScore.position.x = size.x - 15.0;
    add(player);
    add(roomName);
    add(playerScore);
    loadLevel(generateLevel(this));
  }

  void loadLevel(List<PickUp> goals) {
    for (var e in objects) remove(e);
    for (var e in goals) add(e);
    objects = goals;
  }

  @override
  void onPanUpdate(DragUpdateInfo info) {
    player.move(info.delta.game);
  }

  @override
  KeyEventResult onKeyEvent(RawKeyEvent raw, Set<dynamic> pressed) {
    final isW = pressed.contains(LogicalKeyboardKey.keyW) ? -1 : 0;
    final isA = pressed.contains(LogicalKeyboardKey.keyA) ? -1 : 0;
    final isS = pressed.contains(LogicalKeyboardKey.keyS) ? 1 : 0;
    final isD = pressed.contains(LogicalKeyboardKey.keyD) ? 1 : 0;
    if (raw is RawKeyDownEvent) {
      drop_off = (isW + isS) + (isA + isD);
      moving.y += 150.0 * (isW + isS);
      moving.x += 150.0 * (isA + isD);
      moving.clamp(Vector2.all(-300.0), Vector2.all(300));
      return KeyEventResult.handled;
    }
    if (raw is RawKeyUpEvent) drop_off -= 1;
    if (drop_off < 0) drop_off = 0;
    return KeyEventResult.ignored;

    //todo move player
  }

  @override
  void update(double dt) {
    super.update(dt);
    roomName.text = "${player.curRoom + 1}";
    roomName.update(dt);
    playerScore.text = "${player.score}";
    playerScore.update(dt);
    player.move(moving * dt);
    if (drop_off == 0) moving *= (0.90);
    for (var e in objects) e.visible = (e.room == player.curRoom);
  }

  @override
  void onTap() {
    super.onTap();
    var p = null;
    for (var e in objects) {
      if (player.containsPoint(e.position) && e.visible) {
        player.score += 1;
        p = e;
        remove(e);
      }
    }
    if (p is PickUp) objects.remove(p);
    if (objects.isEmpty) loadLevel(generateLevel(this));
  }
}
