import 'package:detective/game_world.dart';
import 'package:detective/pick_up.dart';
import 'dart:math';

List<PickUp> generateLevel(DetectiveGame game) {
  var rng = Random();
  int count = rng.nextInt(15);
  int roomMax = game.rooms.x.toInt() * game.rooms.y.toInt();
  List<PickUp> out = [];
  for (int i = 0; i < count; ++i)
    out.add(PickUp()
      ..room = rng.nextInt(roomMax)
      ..position.x = rng.nextInt(game.size.x.toInt() - 15) + 5
      ..position.y = rng.nextInt(game.size.y.toInt() - 15) + 5);
  return out;
}
