import 'package:detective/game_world.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:flame/flame.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  try {
    await Flame.device.setLandscape();
  } catch (e) {}

  late final game = DetectiveGame();
  runApp(GameWidget(game: game));
}
