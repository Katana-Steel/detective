import 'package:flame/components.dart';
import 'package:detective/game_world.dart';
import 'package:flutter/rendering.dart';

class PickUp extends SpriteComponent with HasGameRef<DetectiveGame> {
  bool visible = false;
  int room = -1;
  String val = "";
  @override
  Future<void>? onLoad() async {
    sprite = await gameRef.loadSprite('yellow.png');
    size = Vector2.all(10);
    return super.onLoad();
  }

  @override
  void render(Canvas canvas) {
    if (visible) super.render(canvas);
  }
}
