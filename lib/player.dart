import 'package:flame/components.dart';
import 'package:detective/game_world.dart';

enum Direction {
  None,
  North,
  South,
  East,
  West,
}

class Player extends SpriteComponent with HasGameRef<DetectiveGame> {
  int curRoom;
  int score = 0;
  Player(int r)
      : curRoom = r,
        super(size: Vector2.all(50.0));

  @override
  Future<void>? onLoad() async {
    await super.onLoad();
    sprite = await gameRef.loadSprite('red.png');
    position = gameRef.size / 2;
    height = 25;
    width = 25;
    anchor = Anchor.center;
  }

  bool canMove(Direction direction) {
    int cols = gameRef.rooms.x.toInt();
    int rows = gameRef.rooms.y.toInt();
    int row = (curRoom ~/ cols) % rows;
    int col = curRoom % cols;
    switch (direction) {
      case Direction.North:
        return row > 0;
      case Direction.East:
        return col < (cols - 1);
      case Direction.West:
        return col > 0;
      case Direction.South:
        return row < (rows - 1);
      default:
        return true;
    }
  }

  Direction move(Vector2 delta) {
    position.add(delta);
    if (position.x > gameRef.size.x) {
      if (canMove(Direction.East)) {
        curRoom += 1;
        position.x = 0.0001;
        return Direction.East;
      } else {
        position.x = (gameRef.size.x - 0.0001);
      }
    }
    if (position.y > gameRef.size.y) {
      if (canMove(Direction.South)) {
        curRoom += gameRef.rooms.x.toInt();
        position.y = 0.0001;
        return Direction.South;
      } else {
        position.y = (gameRef.size.y - 0.0001);
      }
    }
    if (position.x < 0) {
      if (canMove(Direction.West)) {
        curRoom -= 1;
        position.x = (gameRef.size.x - 0.0001);
        return Direction.West;
      } else {
        position.x = 0.0001;
      }
    }
    if (position.y < 0) {
      if (canMove(Direction.North)) {
        curRoom -= gameRef.rooms.x.toInt();
        position.y = (gameRef.size.y - 0.0001);
        return Direction.North;
      } else {
        position.y = 0.0001;
      }
    }
    return Direction.None;
  }
}
